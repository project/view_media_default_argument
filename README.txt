
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

View Media Default Arguments module provide default values for contextual 
filters.This can be useful for blocks and other display types lacking a 
natural argument input.This module allows you to provide media IDs and 
taxonomy term Ids as default argument from the URL.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/view_media_default_argument
 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/view_media_default_argument

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

 

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no configuration settings. 

MAINTAINERS
-----------

Current maintainers:
 * Ashutosh Mishra (ashutosh.mishra) - https://www.drupal.org/user/3565900
